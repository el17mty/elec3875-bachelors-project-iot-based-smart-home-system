import wiringpi, sys, time
wiringpi.wiringPiSetup()
serial = wiringpi.serialOpen('/dev/ttyS0',4800)
if len(sys.argv) == 1:
    print("Usage: python Transmit.py [argument1] [argument2] ...")
else:
    for argument in sys.argv:
        if argument != "Transmit.py":
            wiringpi.serialPrintf(serial, argument)
            print("Message Sent: " + argument)
            time.sleep(1)
    print("Transfer Done!")
wiringpi.serialClose(serial)
