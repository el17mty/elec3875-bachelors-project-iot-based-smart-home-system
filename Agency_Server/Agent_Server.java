import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Agent_Server {
  // Main function for the server
  public static void main( String[] args ) throws IOException {
    ServerSocket serverSocket = null;
    ExecutorService service = null;

    // Liseten on port 8888 with error handling
    try {
      serverSocket = new ServerSocket(8888);
    } catch (IOException err) {
      System.err.println("Could not listen on port: 8888.");
      System.exit(-1);
    }

    System.out.println("Running Server on port " + serverSocket.getLocalPort() + "\n");
    // Use an executors to manage a fixed thread-pool with 2 client connection
    service = Executors.newFixedThreadPool(2);
    // Run the service continuously, once the connection has been accepted
    // Then, assign the Connection_Handler to the executor
    while (true) {
      Socket client = serverSocket.accept();
      service.submit(new Connection_Handler(client));
    }
  }
}
