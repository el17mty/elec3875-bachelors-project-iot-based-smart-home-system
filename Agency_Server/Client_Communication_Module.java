import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client_Communication_Module {

  // List of state variables with their default values
  private Socket serverSocket = null;
  private DataOutputStream socketOutput = null;
  private DataInputStream socketInput = null;



  // Method that send the message to server
  public void sendMessageToServer(String message) {
    // Creating socket with ip address and port number
    // Then, chain the socket I/O steam to the Data I/O steam
    try {
      serverSocket = new Socket("localhost", 8888);
      socketOutput = new DataOutputStream(serverSocket.getOutputStream());
      socketInput = new DataInputStream(serverSocket.getInputStream());
    } catch (UnknownHostException err) {
      System.err.println("Can't resolve host during creating socket");
      System.exit(1);
    } catch (IOException err) {
      System.err.println("I/O Exception during creating socket and I/O stream");
      System.exit(1);
    }

    try{
      socketOutput.writeUTF(message);
    } catch (IOException err) {
      System.err.println("I/O Exception during sending message");
      System.exit(1);
    }

    // Close all the I/O stream and network socket in the end of the program
    try {
      socketInput.close();
      socketOutput.close();
      serverSocket.close();
    } catch (IOException err) {
      System.err.println("I/O Exception during closing socket and I/O stream");
      System.exit(1);
    }
  }



  public static void main( String[] args )
  {
    String messageToServer = "";
    // Create a new Client object onec the program in executed
    Client_Communication_Module client = new Client_Communication_Module();
    for (String str : args) {
      messageToServer += str + " ";
    }
    // The program accept both command line argument and input after the program run
    // Check there is input arugments or not
    if (args.length > 0) {
      client.sendMessageToServer(messageToServer.trim());
    } else {
      // Promt the user to input the messageToServer if they didn't input as arguments.
      // Create a scanner to read terminal input for the messageToServer.
      Scanner keyboardIn = new Scanner(System.in);
      System.out.print("Message to Server: ");
      messageToServer = keyboardIn.nextLine();
      client.sendMessageToServer(messageToServer.trim());
      keyboardIn.close();
    }
  }
}