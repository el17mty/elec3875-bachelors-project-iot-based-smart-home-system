import java.io.*;
import java.util.concurrent.TimeUnit;

public class Message_Encode_And_Decoder {
  private String loginPassword = "12345678";
  private String temp = null;
  private String lux = null;
  private String hum = null;
  public String decodeMessage(String message) {
    if (message.contains("login")) {
      String receivedPassword = message.split(";")[1];
      if(loginPassword.equals(receivedPassword)) {
        System.out.println("Login Successful");
        return "success";
      } else {
        System.out.println("Login Incorrect");
        return "incorrect";
      }
    } else if (message.contains("fetch")) {
      String s = null;
      try {
        Process pr = Runtime.getRuntime().exec("python Receive.py");
        BufferedReader stdInput = new BufferedReader(new InputStreamReader(pr.getInputStream()));
        Process p = Runtime.getRuntime().exec("python Transmit.py Nano2:TEMP:");
        System.out.println("Command: python Transmit.py Nano2:TEMP:");
        while ((s = stdInput.readLine()) != null) {
           temp = s;
        }
        pr = Runtime.getRuntime().exec("python Receive.py");
        stdInput = new BufferedReader(new InputStreamReader(pr.getInputStream()));
        p = Runtime.getRuntime().exec("python Transmit.py Nano2:LUX:");
        System.out.println("Command: python Transmit.py Nano2:LUX:");
        while ((s = stdInput.readLine()) != null) {
           lux = s;
        }
        pr = Runtime.getRuntime().exec("python Receive.py");
        stdInput = new BufferedReader(new InputStreamReader(pr.getInputStream()));
        p = Runtime.getRuntime().exec("python Transmit.py Nano2:HUM:");
        System.out.println("Command: python Transmit.py Nano2:HUM:");
        while ((s = stdInput.readLine()) != null) {
           hum = s;
        }
        return temp + ";" + lux + ";" + hum;
      } catch (IOException e) {
        e.printStackTrace();
        System.exit(-1);
      }
    } else if (message.contains("curtain")) { 
      String receivedPercentage = message.split(";")[1];
      String command = "python Transmit.py Uno1:" + receivedPercentage + ":";
      System.out.println("Command: " + command);
      try {
        Process p = Runtime.getRuntime().exec(command);
      } catch (IOException e) {
        e.printStackTrace();
        System.exit(-1);
      }
      return "Nothing";
    } else if (message.contains("led")) { 
      String red = message.split(";")[1];
      String green = message.split(";")[2];
      String blue = message.split(";")[3];
      String command = "python Transmit.py Nano1:LED:" + red + ":" + green + ":" + blue + ":";
      System.out.println("Command: " + command);
      try {
        Process p = Runtime.getRuntime().exec(command);
      } catch (IOException e) {
        e.printStackTrace();
        System.exit(-1);
      }
      return "Nothing";
    } else if (message.contains("usb")) { 
      String onOff = message.split(";")[1];
      if(onOff.equals("on")) {
        try {
          Process p = Runtime.getRuntime().exec("python Transmit.py Nano1:USB:On:");
          System.out.println("Command: python Transmit.py Nano1:USB:On:");
        } catch (IOException e) {
          e.printStackTrace();
          System.exit(-1);
        }
      } else if(onOff.equals("off")) {
        try {
          Process p = Runtime.getRuntime().exec("python Transmit.py Nano1:USB:ff:");
          System.out.println("Command: python Transmit.py Nano1:USB:Off:");
        } catch (IOException e) {
          e.printStackTrace();
          System.exit(-1);
        }
      }
      return "Nothing";
    }
    return "err";
  }
}
