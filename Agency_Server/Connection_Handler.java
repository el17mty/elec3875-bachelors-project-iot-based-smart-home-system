import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Connection_Handler extends Thread {
  // Initial the client socket
  private Socket socket = null;

  // Constructor for this class, used the super class Thread Constructor
  public Connection_Handler(Socket socket) {
    super("Connection_Handler");
    this.socket = socket;
  }

  // The function will run after a connection has made.
  public void run() {
    try {
      // Chain the socket I/O steam to the Data I/O steam
      DataOutputStream socketOutput = new DataOutputStream(socket.getOutputStream());
      DataInputStream socketInput = new DataInputStream(socket.getInputStream());
      PrintWriter printWriter = new PrintWriter(socketOutput, true);
      Message_Encode_And_Decoder message_encode_and_decoder = new Message_Encode_And_Decoder();

      // Read the instruction from the DataInputStream
      InetAddress inet = socket.getInetAddress();
      System.out.println("Connection from address: " + inet.getHostAddress());
      String instruction = socketInput.readUTF();
      System.out.println("Message from Client: " + instruction);
      String response =  message_encode_and_decoder.decodeMessage(instruction);
      System.out.println("Message to Client: " + response + "\n");
      socketOutput.writeUTF(response);

      // Create BufferedWriter object to record connection log to the file log.txt
      BufferedWriter logWriter = new BufferedWriter(new FileWriter(new File("./log.txt"), true));
      // Get the current time with specific format
      String dateTime = new SimpleDateFormat("yyyy/MM/dd:HH:mm:ss").format(Calendar.getInstance().getTime());
      // Combine all the information into sigle log string
      String logText = dateTime + ":" + inet.getHostAddress() + ":" + instruction + "\n";
      // Write the string to the file
      logWriter.write(logText);
      logWriter.flush();


      logWriter.close();
      // Close all I/O steam and socket for this client
      socketInput.close();
      socketOutput.close();
      socket.close();
    } catch (IOException err) {
      System.err.println("Exception during socket accept and IO steam creation");
      System.exit(1);
    }

  }
}
