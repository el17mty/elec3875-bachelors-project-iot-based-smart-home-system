package com.example.iotsmarthomesystem;

import android.os.AsyncTask;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

class  ConnectionTask extends AsyncTask<Void, Void, Void> {

    String SERVER_IP = "localhost";
    int SERVER_PORT = 8888;

    private DataOutputStream socketOutput;
    private DataInputStream socketInput;
    public String messageSend = null;
    public String messageReceived = null;

    ConnectionTask(String ip, String message) {
        this.SERVER_IP = ip;
        this.messageSend = message;
    }

    public void setMessage(String message) {
        this.messageSend = message;
    }

    @Override
    protected  Void doInBackground(Void... parans) {
        Socket serverSocket = null;
        try {
            serverSocket = new Socket(SERVER_IP, SERVER_PORT);
            socketOutput = new DataOutputStream(serverSocket.getOutputStream());
            socketInput = new DataInputStream(serverSocket.getInputStream());
        } catch (UnknownHostException err) {
            System.err.println("Can't resolve host during creating socket");
            err.printStackTrace();
        } catch (IOException err) {
            System.err.println("I/O Exception during creating socket and I/O stream");
            err.printStackTrace();
        }
        try{
            socketOutput.writeUTF(messageSend);
            socketOutput.flush();
            messageReceived = socketInput.readUTF();
        } catch (IOException err) {
            System.err.println("I/O Exception during sending message");
            System.exit(1);
        }
        try {
            socketInput.close();
            socketOutput.close();
            serverSocket.close();
        } catch (IOException err) {
            System.err.println("I/O Exception during closing socket and I/O stream");
            System.exit(1);
        }
        return null;
    }

}
