package com.example.iotsmarthomesystem;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import java.util.concurrent.ExecutionException;


/**
 * A simple {@link Fragment} subclass.
 */
public class StatusFragment extends Fragment {

    public StatusFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_status, container, false);

        //final String IP_ADDRESS = getArguments().getString("edttext");

        final String IP_ADDRESS = "192.168.137.37";

        final Switch usb_Switch = (Switch) view.findViewById(R.id.USBSwitch);
        final SeekBar curtain_Bar = (SeekBar) view.findViewById(R.id.CurtainSeekBar);
        final SeekBar red_Bar = (SeekBar) view.findViewById(R.id.LEDRedSeekBar);
        final SeekBar green_Bar = (SeekBar) view.findViewById(R.id.LEDGreenSeekBar);
        final SeekBar blue_Bar = (SeekBar) view.findViewById(R.id.LEDBlueSeekBar);
        final ImageButton refresh_Button = (ImageButton) view.findViewById(R.id.refreshButton);

        final TextView light_Intensity = (TextView) view.findViewById(R.id.LightIntensityValue);
        final TextView temperature = (TextView) view.findViewById(R.id.TemperatureValue);
        final TextView humidity = (TextView) view.findViewById(R.id.HumidityValue);

        refresh_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectionTask ct = new ConnectionTask(IP_ADDRESS, "fetch;");
                try {
                    ct.execute().get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                String response = ct.messageReceived;
                light_Intensity.setText(response.split(";")[1] + "%");
                temperature.setText(response.split(";")[0] + "°C");
                humidity.setText(response.split(";")[2] + "%");
            }
        });

        curtain_Bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int percentage = seekBar.getProgress();
                ConnectionTask ct = new ConnectionTask(IP_ADDRESS, "curtain;" + percentage);
                try {
                    ct.execute().get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        red_Bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int red = 255 * seekBar.getProgress() / 100;
                int green = 255 * green_Bar.getProgress() / 100;
                int blue = 255 * blue_Bar.getProgress() / 100;
                ConnectionTask ct = new ConnectionTask(IP_ADDRESS, "led;" + red + ";" + green + ";" + blue);
                try {
                    ct.execute().get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        green_Bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int red = 255 * red_Bar.getProgress() / 100;
                int green = 255 * seekBar.getProgress() / 100;
                int blue = 255 * blue_Bar.getProgress() / 100;
                ConnectionTask ct = new ConnectionTask(IP_ADDRESS, "led;" + red + ";" + green + ";" + blue);
                try {
                    ct.execute().get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        blue_Bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int red = 255 * red_Bar.getProgress() / 100;
                int green = 255 * green_Bar.getProgress() / 100;
                int blue = 255 * seekBar.getProgress() / 100;
                ConnectionTask ct = new ConnectionTask(IP_ADDRESS, "led;" + red + ";" + green + ";" + blue);
                try {
                    ct.execute().get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        usb_Switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ConnectionTask ct = null;
                if(isChecked) {
                    ct = new ConnectionTask(IP_ADDRESS, "usb;on");
                } else {
                    ct = new ConnectionTask(IP_ADDRESS, "usb;off");
                }
                try {
                    ct.execute().get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        // Inflate the layout for this fragment
        return view;
    }
}
