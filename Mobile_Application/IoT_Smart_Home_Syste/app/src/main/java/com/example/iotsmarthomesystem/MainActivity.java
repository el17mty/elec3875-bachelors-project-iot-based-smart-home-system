package com.example.iotsmarthomesystem;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    public String SERVER_IP = "localhost";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText input_IP = (EditText) findViewById(R.id.input_IP);
        final EditText input_Password = (EditText) findViewById(R.id.input_Password);
        final Button login_Button = (Button) findViewById(R.id.login_Button);

        login_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SERVER_IP = input_IP.getText().toString().trim();

                if(SERVER_IP.isEmpty() || input_Password.getText().toString().trim().isEmpty()) {
                    Toast.makeText(MainActivity.this, "IP and Password Field Can't be empty", Toast.LENGTH_LONG).show();
                } else {
                    ConnectionTask ct = new ConnectionTask(SERVER_IP, "login;" + input_Password.getText().toString().trim());
                    try {
                        ct.execute().get();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    String response = ct.messageReceived;
                    if (response.equals("success")) {
                        Intent registerIntent = new Intent(MainActivity.this, LoginedActivity.class);
                        MainActivity.this.startActivity(registerIntent);
                    } else if (response.equals("incorrect")) {
                        Toast.makeText(MainActivity.this, "Incorrect Password :(", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    public String getMyData() {
        return SERVER_IP;
    }
}
